﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MiLogger.NivelLog
{
    public interface INivelLog
    {
        string DecorarMensajeLog(string mensajeALoguear);
        void InicializaConsola();
    }
}
