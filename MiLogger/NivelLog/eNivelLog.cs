﻿namespace MiLogger.NivelLog
{
    public enum eNivelLog
    {
        Informacion,
        Alerta,
        Error
    }
}