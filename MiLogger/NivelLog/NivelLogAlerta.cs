﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiLogger.NivelLog
{
    public class NivelLogAlerta : INivelLog
    {
        public string DecorarMensajeLog(string mensajeALoguear)
        {
            return "[ALERTA] " + mensajeALoguear;
        }
        public void InicializaConsola()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
        }
    }
}
