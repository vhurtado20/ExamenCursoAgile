﻿using MiLogger.NivelLog;
using System;
using System.Configuration;
using System.IO;

namespace MiLogger.TargetLog
{
    public class LoggerTexto : LoggerBase
    {
        public LoggerTexto()
            : base()
        {
        }
        protected override void EjecutarLog(INivelLog nivel)
        {
            string directorioDestinoParaLog = ConfigurationManager.AppSettings["CarpetaDeLog"];
            string rutaArchivoLogTexto = Path.Combine(directorioDestinoParaLog, "LogTexto" + DateTime.Now.ToShortDateString().Replace("/","") + ".txt");
            if (!Directory.Exists(directorioDestinoParaLog))
            {
                CrearDirectorioParaLog(directorioDestinoParaLog);
            }
            if (!File.Exists(rutaArchivoLogTexto))
            {
                CrearArchivoTxtVacio(rutaArchivoLogTexto);
            }
            using (StreamWriter sw = File.AppendText(rutaArchivoLogTexto))
            {
                sw.WriteLine(this._textoALoguear);
            }
        }
        private void CrearDirectorioParaLog(string rutaDirectorioParaLog)
        {
            Directory.CreateDirectory(rutaDirectorioParaLog);
        }
        private void CrearArchivoTxtVacio(string rutaArchivoTexto)
        {
            using (StreamWriter sw = File.CreateText(rutaArchivoTexto))
            {
            }
        }
    }
}
