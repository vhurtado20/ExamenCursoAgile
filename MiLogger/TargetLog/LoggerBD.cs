﻿using MiLogger.NivelLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace MiLogger.TargetLog
{
    public class LoggerBD : LoggerBase
    {
        private IDbCommand _comandoBD;
        public LoggerBD() : base()
        {
        }
        public LoggerBD(IDbCommand _comandoBD)
            : base()
        {
            this._comandoBD = _comandoBD;
        }

        protected override void EjecutarLog(INivelLog nivel)
        {
            SqlConnection conexionSql = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            using (conexionSql)
            {
                _comandoBD.CommandText = "Insert into Log Values('" + this._textoALoguear + "')";
                _comandoBD.ExecuteNonQuery();
            }
        }
    }
}
