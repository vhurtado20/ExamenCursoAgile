﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Channels;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using MiLogger.NivelLog;

namespace MiLogger.TargetLog
{
    public abstract class LoggerBase
    {
        protected string _textoALoguear;

        public void Informacion(string textoALoguear)
        {
            PreProcesarTextoLog(textoALoguear);
            this._textoALoguear = new NivelLogInformacion().DecorarMensajeLog(this._textoALoguear);
            EjecutarLog(new NivelLogInformacion());
        }
        public void Alerta(string textoALoguear)
        {
            PreProcesarTextoLog(textoALoguear);
            this._textoALoguear = new NivelLogAlerta().DecorarMensajeLog(this._textoALoguear);
            EjecutarLog(new NivelLogAlerta());
        }
        public void Error(string textoALoguear)
        {
            PreProcesarTextoLog(textoALoguear);
            this._textoALoguear = new NivelLogError().DecorarMensajeLog(this._textoALoguear);
            EjecutarLog(new NivelLogError());
        }

        protected abstract void EjecutarLog(INivelLog nivel);

        private void AgregarFechaHoraATextoALoguear()
        {
            this._textoALoguear = this._textoALoguear + " " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();
        }
        private void PreProcesarTextoLog(string textoALoguear)
        {
            this._textoALoguear = textoALoguear;
            ValidarTextoLog();
            FormatearTextoLog();
        }
        private void ValidarTextoLog()
        {
            if (string.IsNullOrEmpty(this._textoALoguear))
            {
                throw new ArgumentException();
            }
        }
        private void FormatearTextoLog()
        {
            AgregarFechaHoraATextoALoguear();
        }
    }
}
