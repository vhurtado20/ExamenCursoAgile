﻿using MiLogger.NivelLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace MiLogger.TargetLog
{
    public class LoggerConsola : LoggerBase
    {
        public LoggerConsola()
            : base()
        {
        }
        protected override void EjecutarLog(INivelLog nivel)
        {
            nivel.InicializaConsola();
            Console.WriteLine(this._textoALoguear);
        }
    }
}
