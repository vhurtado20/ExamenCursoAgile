﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MiLogger.NivelLog;
using MiLogger.TargetLog;
using NUnit.Framework;

namespace MiLogger.UnitTesting
{
    [TestFixture]
    public class LoggerConsolaTest
    {
        private string _textoALoguear = "Log de ejemplo";
        [Test]
        public void Logger_Creacion_SeCreaInstancia()
        {
            LoggerConsola logger = new LoggerConsola();
        }
        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void Logger_DadoTextoVacioInfo_SeDisparaException()
        {
            LoggerConsola logger = new LoggerConsola();
            logger.Informacion(string.Empty);
        }
        [Test]
        public void Logger_LoguearConsolaInfo_SeLogueaInfoEnConsola()
        {
            LoggerConsola logger = new LoggerConsola();
            logger.Informacion(this._textoALoguear);
        }
        [Test]
        public void Logger_LoguearConsolaAlerta_SeLogueaAlertaEnConsola()
        {
            LoggerConsola logger = new LoggerConsola();
            logger.Alerta(this._textoALoguear);
        }
        [Test]
        public void Logger_LoguearConsolaError_SeLogueaErrorEnConsola()
        {
            LoggerConsola logger = new LoggerConsola();
            logger.Error(this._textoALoguear);
        }
    }
}
