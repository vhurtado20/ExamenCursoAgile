﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using MiLogger.TargetLog;
using MiLogger.NivelLog;
using Moq;

namespace MiLogger.UnitTesting
{
    [TestFixture]
    public class LoggerBDTest
    {
        private string _textoALoguear = "Log de ejemplo";
        private Mock<IDbCommand> _comandoBD;
        [SetUp]
        public void InicializaVariablesParaPruebas()
        {
            _comandoBD = new Mock<IDbCommand>();
            _comandoBD.Setup(x => x.ExecuteNonQuery());
        }

        [TearDown]
        public void LimpiaVariablesParaPruebas()
        {
            _comandoBD = null;
        }

        [Test]
        public void Logger_Creacion_SeCreaInstancia()
        {
            LoggerBD logger = new LoggerBD();
        }
        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void Logger_DadoTextoVacioInfo_SeDisparaException()
        {
            LoggerBD logger = new LoggerBD();
            logger.Informacion(string.Empty);
        }

        [Test]
        public void Logger_LoguearBDInfo_SeLogueaInfoEnBD()
        {
            LoggerBD logger = new LoggerBD(_comandoBD.Object);
            logger.Informacion(this._textoALoguear);
        }
        [Test]
        public void Logger_LoguearEnBDAlerta_SeLogueaAlertaEnBD()
        {
            LoggerBD logger = new LoggerBD(this._comandoBD.Object);
            logger.Alerta(this._textoALoguear);
        }
        [Test]
        public void Logger_LoguearEnBDError_SeLogueaErrorEnBD()
        {
            LoggerBD logger = new LoggerBD(this._comandoBD.Object);
            logger.Error(this._textoALoguear);
        }
    }
}
