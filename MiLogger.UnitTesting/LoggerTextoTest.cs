﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MiLogger.NivelLog;
using MiLogger.TargetLog;
using NUnit.Framework;

namespace MiLogger.UnitTesting
{
    [TestFixture]
    public class LoggerTextoTest
    {
        private string _textoALoguear = "Log de ejemplo";
        [Test]
        public void Logger_Creacion_SeCreaInstancia()
        {
            LoggerTexto logger = new LoggerTexto();
        }
        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void Logger_DadoTextoVacioInfo_SeDisparaException()
        {
            LoggerTexto logger = new LoggerTexto();
            logger.Informacion(string.Empty);
        }
        [Test]
        public void Logger_LoguearTextoInfo_SeLogueaInfoEnTexto()
        {
            LoggerTexto logger = new LoggerTexto();
            logger.Informacion(this._textoALoguear);
        }
        [Test]
        public void Logger_LoguearTextoAlerta_SeLogueaAlertaEnTexto()
        {
            LoggerTexto logger = new LoggerTexto();
            logger.Alerta(this._textoALoguear);
        }
        [Test]
        public void Logger_LoguearTextoError_SeLogueaErrorEnTexto()
        {
            LoggerTexto logger = new LoggerTexto();
            logger.Error(this._textoALoguear);
        }
    }
}
